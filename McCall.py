# -*- coding: utf-8 -*-
"""
Created on Thu Feb 18 16:13:52 2016

@author: David Evans
"""

import numpy as np

def bellmanTmap(beta,w,pi,b,alpha,chi,Vcont,Qcont):
    '''
    Iterates of Bellman Equation in Problem 1
    See Homework 3.pdf for Documentation
    '''
    pass
    
    
    
def solveInfiniteHorizonProblem(beta,w,pi,b,alpha,chi,epsilon=1e-8):
    '''
    Solve infinite horizon workers problem in Problem 1
    See Homework 3.pdf for Documentation
    '''
    pass
    
def HazardRate(pi,C,E):
    '''
    Computes the hazard rate of leaving unemployment in Problem 1
    See Homework 3.pdf for Documentation
    '''
    pass
    

        
        
def bellmanTmap_HC(beta,w,pi,b,alpha,p_h,h,Vcont,Qcont):
    '''
    Iterates of Bellman Equation of Problem 2
    See Homework 3.pdf for Documentation
    '''
    pass

def solveInfiniteHorizonProblem_HC(beta,w,pi,b,alpha,hprob,hgrid,epsilon=1e-8):
    '''
    Solve infinite horizon workers problem of Problem 2
    See Homework 3.pdf for Documentation
    '''
    pass

    
    
    
    
    
    
    
    